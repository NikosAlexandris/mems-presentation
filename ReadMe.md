# About

One of the research topics of the Natural Capital Soil project (European
Commission, Join Research Center, Land Resources Unit D3), is soil organic
carbon modelling.  The Colorado State University (CSU) develops a relevant
model and sofware too named [MEMS](https://doi.org/10.5194/bg-2018-430).

The MEMS software (not yet publicly available) calculates the impact of
changing management and environmental conditions on elemental cycling in Soil
Organic Matter.

# Presentations

- [Presentation of the project](https://gitpitch.com/NikosAlexandris/mems-presentation/master?p=presentation&grs=gitlab) at the 1st JRC Big Data Platform (JEODPP) 07. 03. 2019
- [Progress](https://gitpitch.com/NikosAlexandris/mems-presentation/master?p=progress&grs=gitlab) at the 2nd JRC Big Data Platform (JEODPP) 25. 06. 2019

### Notes

* [gitpitch](https://gitpitch.com/)
will _not_ work
on self-hosted git servers
unless a license for
[Enterprise Self Hosted Service](https://gitpitch.com/docs/about/enterprise)
is obtained.
* Looking for a PDF of a _gitpitch_ presentation?
Read [Online Printing](https://gitpitch.com/docs/foundation-features/pdf/#online-printing)
