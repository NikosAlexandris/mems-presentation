---?image=presentation/img/mems_biogeochemistry_model.jpg&position=right&size=contain
@title[Overview]

@snap[north-west span-60]
[@color[brown](@size[2em](MEMS))](https://doi.org/10.5194/bg-2018-430)<br>
@color[blue](=) Microbial Efficiency-Matrix Stabilization<br>
@size[0.85em](@color[blue](*)a @color[green](bio-)@color[magenta](geochemistry) model)
@snapend

@snap[west span-60]
<br><br><br>@size[0.7em](impact of changing management & environmental conditions on elemental cycling in @color[brown](Soil Organic Matter) (@color[gray](SOM)))
@snapend

@snap[south-west text-05 span-65]
@color[gray](Colorado State University [Department of Soil & Crop Sciences; Natural Resources Ecology Laboratory; Department of Ecosystem Science & Sustainability]<br>European Commission, @color[blue](JRC, Land Resources Unit D3))
@snapend

+++
@title[Software]
@snap[north span-85]
@box[bg-yellow text-blue rounded](Software#v1.0 is a Java library<br><br>Not yet Public!<br><br>Site-specific or Pixel-based)
@snapend

@snap[south span-85]
@box[bg-blue text-yellow](Global trials#<br>5km  ~15MP<br><br>1km ?)
@snapend

---?include=presentation/md/input/PITCHME.md

---?include=presentation/md/process/PITCHME.md

---?include=presentation/md/output/PITCHME.md

---?include=presentation/md/resources/PITCHME.md

---?include=presentation/md/about/PITCHME.md
