---?image=presentation/img/bg/purple.jpg&color=white&position=top&size=100% 50%
@title[Who?]

@snap[north text-white span-100]
@size[1.5em](Who is doing what?)
@snapend

@snap[west about-team-pic]
![Nikos](presentation/img/profile/nik.jpg)
@snapend

@snap[south-west text-06]
@color[#4487F2](Nikos Alexandris)
<br><br>
@fa[twitter](NikAlexandris)
<br>
Remote Sensing & Geomatics
@snapend

@snap[east about-team-pic]
![Emanuele](presentation/img/profile/emanuele.jpg)
@snapend

@snap[south-east text-06]
@color[#4487F2](Emanuele Lugato)
<br><br>
@fa[twitter](EmanueleLugato)
<br>
Researcher
@snapend
