---?image=presentation/img/soilgrids.org_soil_organic_carbon_content_in_g_per_kg_2019-03-06_23-40_xs.jpg&size=contain
@title[Raster in]

@snap[center]
@box[bg-white text-purple rounded](SoilGrids.org# Bulk density, Sand, Clay, Rock, pH<br> and Soil Organic Carbon  @color[gray](currently for validation))
@snapend

+++
@title[Parameters]

<!-- @snap[north] -->
<!-- @color[gray](from raster images to CSV) -->
<!-- @snapend -->

@color[blue](Input) is a @color[blue](Control) file reading parameters for:

* Site
* Soil
* Climate

@fa[arrow-down text-black]

+++?code=presentation/src/csv/in_Control_head_18.csv
@title[CSV in]

@[18](No daily result due to @color[gold](`DailyOutput`) set to @color[red](`N`) in the Control file)

@snap[north-west text-15 text-bold text-gray]
Control
@snapend

+++?code=presentation/src/csv/in_Site_head_18.csv
@title[Site]

@snap[north-west text-15 text-bold text-gray]
Site
@snapend

+++?code=presentation/src/csv/in_Soil_head_18.csv
@title[Soil]

@snap[north-west text-15 text-bold text-gray]
Soil
@snapend

+++?code=presentation/src/csv/in_Weather_head_18.csv
@title[Weather]

@snap[north-west text-15 text-bold text-gray]
Weather
@snapend
