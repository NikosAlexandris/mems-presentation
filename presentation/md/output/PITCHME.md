---?image=presentation/img/global_soil_organic_carbon_map.jpg&size=contain
@title[Raster out]

+++
@title[CSV out]

<!-- @snap[north] -->
<!-- @color[gray](from CSV to raster images) -->
<!-- @snapend -->

@color[blue](Output) are @color[blue](CSV) files...

@fa[arrow-down text-black]

+++?code=presentation/src/csv/yearlySOM_head_18.csv
@title[Output Yearly Soil OM]

@snap[north-east text-15 text-bold text-gray]
Yearly Soil OM
@snapend

+++?code=presentation/src/csv/yearlySurfOM_head_18.csv
@title[Output Yearly Surface OM]

@snap[north-east text-15 text-bold text-gray]
Yearly Surface OM
@snapend

+++?code=presentation/src/csv/dailySOM_head_18.csv
@title[Output Daily Soil OM]

@[1](No daily result due to @color[gold](`DailyOutput`) set to @color[red](`N`) in the Control file)

@snap[north-east text-15 text-bold text-gray]
Daily Soil OM
@snapend

+++?code=presentation/src/csv/dailySurfOM_head_18.csv
@title[Output Daily Surface OM]

@[1](No daily result due to @color[gold](`DailyOutput`) set to @color[red](`N`) in the Control file)

@snap[north-east text-15 text-bold text-gray]
Daily Surface OM
@snapend

