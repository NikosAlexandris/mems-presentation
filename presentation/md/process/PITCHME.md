---?image=presentation/img/bg/yellow.jpg&position=bottom&size=100% 50%
@title[Process + Timing]

@snap[north span-70]
Run for a pixel:<br><br>
```
java MEMS Control.csv
```
@snapend

@snap[south span-70 text-blue fragment ]
@color[purple](Simulating 730485 days)

@ul[bullets](false)
- reading:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0.082833967 s
- calculations:&nbsp;9.635740466 s
- writting:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0.124680701 s
- execution:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;9.843255134 s
@ulend

@snapend
