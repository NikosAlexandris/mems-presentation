@title[Overview]

@snap[north-west span-80]
[@color[brown](@size[2em](MEMS))](https://doi.org/10.5194/bg-2018-430)<br>
<br>
@color[blue](=) @color[brown](M)icrobial @color[brown](E)fficiency-@color[brown](M)atrix @color[brown](S)tabilization<br>
<br>
@color[blue](*) a @color[green](bio-)@color[magenta](geochemistry) model<br>
<br>
@color[blue](considers) impact of management & environmental conditions in @color[brown](Soil Organic Matter)
@snapend

@snap[south-west text-05]
@color[gray](Colorado State University)<br>
@color[gray](@size[0.6em](Dept. of Soil & Crop Sciences | Natural Resources Ecology Laboratory | Dept. of Ecosystem Science & Sustainability))<br>
European Commission, @color[blue](JRC)<br>
@color[blue](@size[0.6em](Land Resources Unit D3))
@snapend

---?include=progress/md/example-output/PITCHME.md

---?include=progress/md/proof-of-concept/PITCHME.md

---?include=progress/md/resources/PITCHME.md

---?include=progress/md/to-do/PITCHME.md

---?include=progress/md/about/PITCHME.md
