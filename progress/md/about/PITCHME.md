---?color=linear-gradient(to top left, #ffe6cd 50%, white 50%)
@title[Who?]

@snap[north text-white span-100]
@size[1.5em](Who is doing what?)
@snapend

@snap[north-west about-team-pic]
<!-- ![Nikos](progress/img/profile/nik.jpg) -->
<br>
@size[0.7em](@color[#4487F2](Nikos Alexandris))
<br>
@fa[twitter](NikAlexandris)
<br>
@size[0.7em](Remote Sensing & Geomatics)
@snapend

@snap[south-east about-team-pic]
@size[0.7em](@color[#4487F2](Emanuele Lugato))
<br>
@fa[twitter](EmanueleLugato)
<br>
@size[0.7em](Soil Science)
<br>
<!-- ![Emanuele](progress/img/profile/emanuele.jpg) -->
@snapend
