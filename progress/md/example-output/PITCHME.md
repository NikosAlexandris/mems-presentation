---?image=progress/img/output/yearly_soil_organic_carbon_simulations_00002_xsmall.jpg&size=contain&opacity=20
@title[Example product]

@snap[north text-bold]
@snapend

@snap[south span-100]
@color[#704214](Mineral Carbon) maps | @color[#ac8a68](Light is low), @color[#482a0c](dark is high)
@snapend

+++?image=progress/img/output/yearly_soil_organic_carbon_simulations_00001_xsmall.jpg&size=contain
@title[Yearly Soil Organic Matter]

@snap[south-east text-15 text-bold text-gray]
0 - 5 cm
@snapend

@snap[center]
@color[magenta](Why is this map empty?)
@snapend

+++?image=progress/img/output/yearly_soil_organic_carbon_simulations_00002_xsmall.jpg&size=contain
@title[Yearly Soil Organic Matter]

@snap[south-east text-15 text-bold text-gray]
5 - 15 cm
@snapend

+++?image=progress/img/output/yearly_soil_organic_carbon_simulations_00003_xsmall.jpg&size=contain
@title[Yearly Soil Organic Matter]

@snap[south-east text-15 text-bold text-gray]
15 - 30 cm
@snapend

+++?image=progress/img/output/yearly_soil_organic_carbon_simulations_00004_xsmall.jpg&size=contain
@title[Yearly Soil Organic Matter]

@snap[south-east text-15 text-bold text-gray]
30 - 60 cm
@snapend

+++?image=progress/img/output/yearly_soil_organic_carbon_simulations_00005_xsmall.jpg&size=contain
@title[Yearly Soil Organic Matter]

@snap[south-east text-15 text-bold text-gray]
60 - 100 cm
@snapend

+++?image=progress/img/output/yearly_soil_organic_carbon_simulations_00006_xsmall.jpg&size=contain
@title[Yearly Soil Organic Matter]

@snap[south-east text-15 text-bold text-gray]
100 - 200 cm
@snapend

+++
@title[Use|r|s]
@snap[north text-15 text-bold text-gray]
Use@color[gray]([@color[red](r)])s
@snapend

* Global @color[brown](Soil Organic Carbon) stocks
* Product(s) to @color[green](use)/@color[blue](compare) with other data sets
* @color[magenta](Tool) for land management & climate interactions
