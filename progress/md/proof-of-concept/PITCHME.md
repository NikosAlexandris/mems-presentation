---?image=progress/img/bg/sepia_light.jpg&position=bottom&size=100%
@title[Proof of Concept]

@snap[north-west text-15 text-bold text-gray]
In
@snapend
* SoilGrids & Metadata
* Various fixed initialisation values

+++
@title[Process/Tools]
@snap[north text-15 text-bold text-gray]
Process / Tools
@snapend

* @color[red](5971827) pixels
* @color[purple](Simulations over 2000 years)
* HTCondor
* @color[magenta](soilgridsmeta) @color[gray]((in-house @color[green](Python) package))
* MEMS @color[gray](Java)
* [@color[green](@size[1em](mlr))](https://github.com/johnkerl/miller) @color[gray]((pronounced: Miller))

@snap[south span-100]
Details @ @color[brown](mems-simulations)'
[@color[blue](@size[1em](ReadMe))](https://cidportal.jrc.ec.europa.eu/apps/gitlab/use_cases/soil-naca/mems/mems-simulations/blob/8255a9759d4277e4604393b3a7b4df738800d982/ReadMe.md)
file
@snapend

+++?code=presentation/src/csv/yearlySOM_head_18.csv
@title[CSV output]

@snap[north-east text-15 text-bold text-gray]
Out
@snapend
