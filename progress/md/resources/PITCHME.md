---?image=progress/img/process/HTCondor_MEMS_jobs_May_2019.png&size=contain
@title[Resources]

@snap[south]
@box[bg-white text-purple rounded](Jobs# 3000 jobs | 2000 pixels / job)
@snapend

+++?code=progress/src/submit_mems_simulation_jobs
@title[Submission file]
@snap[north-west text-15 text-bold text-gray]
Submission file
@snapend

+++?code=progress/src/simulate_mems.sh
@title[Executable script]
@snap[north-west text-15 text-bold text-gray]
Executable script
@snapend
