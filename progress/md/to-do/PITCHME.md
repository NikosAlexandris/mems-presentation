---?image=progress/img/bg/sepia_light.jpg&position=bottom&size=100%
@title[To Do]

@snap[north text-15 text-bold text-gray]
@color[magenta](To Do)
@snapend

* Pre-install required software in docker image
* Improve CSV collection with [@color[green](@size[1em](mlr))](https://github.com/johnkerl/miller) or @color[blue](dask) @color[gray]((ex. by Lorenzo Marasco))
* See [@color[blue](@size[1em](ticket #10))](https://cidportal.jrc.ec.europa.eu/apps/gitlab/use_cases/soil-naca/mems/mems-simulations/issues/10)
in
[@color[brown](@size[1em](mems-simulations))](https://cidportal.jrc.ec.europa.eu/apps/gitlab/use_cases/soil-naca/mems/mems-simulations/)
