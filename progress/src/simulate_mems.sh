while read PAIR ;do

    echo "Simulation $JOB/$LAST_LINE, Pixel: $COLUMN, $ROW"
    JOB=$((JOB + 1))
    set -- $PAIR
    COLUMN=$1  # echo "Column: $COLUMN"
    ROW=$2     # echo "Row: $ROW"

    soilgridsmeta csv \
        --quiet --overwrite \
        --metadata $SOILGRIDS_METADATA \
        $PATH_TO_SOILGRIDS/ \
        $COLUMN $ROW \
        -f $IN_SOIL_CSV

    java MEMS -control $MODEL_CONTROL_FILENAME -out $SCRATCH
    mlr \
        --rs lf \
        --csv --ocsv \
        put '$x='"$COLUMN"'; $y='"$ROW"'' \
        ${YEARLY_SOIL}.csv > ${YEARLY_SOIL}_${COLUMN}_${ROW}.csv

done <<< $COORDINATES

mlr \
    --rs lf \
    --csv --ocsv \
    cat then cut -f "$YEARLY_SOIL_FIELDS_OF_INTEREST" \
    ${YEARLY_SOIL}_*_*.csv > "${PATH_TO_MEMS_OUTPUT}/${YEARLY_SOIL_OUTPUT_CSV}"
